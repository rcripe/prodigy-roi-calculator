$(function() {
/* _____________________________________________

      CONSTANTS
  ______________________________________________ */
let RDL = 0.06981;
let RDM = 0.1225224;
let RDH = 0.18056694;

let LOSL = 0.00106568539527384;
let LOSM = 0.0010061560316808;
let LOSH = 0.00208087490402586;

let COSTL = 2.571;
let COSTM = 2.3774;
let COSTH = 11.64444;
let SIL = 0.3412;
let SIM = 0.357;
let SIH = 0.3018;

  /* _____________________________________________

      VARIABLES
  ______________________________________________ */

var i,s;
var r;
var cost,los,rdevent;




  /* _____________________________________________

      NAVIGATION AND PROGRESS BAR FUNCTIONALITY
  ______________________________________________ */
  // hide progress bar initially
  var progressBar = document.getElementById("progressBar");
  var fieldsets = document.getElementsByTagName("fieldset");

  // create arr for fieldsets
  var myFieldsets = [];
  for (var i = 0; i < fieldsets.length; i++) {
    myFieldsets.push(fieldsets[i]);
  }

  // show first fieldset
  var formStart = 0;
  var firstFieldset = fieldsets[formStart];
  $(firstFieldset).addClass('is-visible');

  // create and store bullets
  var bullet = "<div class='bullet'><span class='bullet-o'></span></div>";
  var numScreens = myFieldsets.length;
  for (var i = 0; i < numScreens - 1; ++i) {
    bullet += "<div class='bullet'><span class='bullet-line'></span><span class='bullet-o'></span></div>";
  };

  // inject bullets
  var progressBar = document.getElementById("progressBar");
  for(var i = 0; i < numScreens; i++) {
    progressBar.innerHTML = bullet;
  }

  // activate first bullet
  var bullets = document.getElementsByClassName("bullet");
  var firstBullet = bullets[0];
  firstBullet.className = "bullet is-active";

  // create array for bullets
  var myBullets = [];
  for (var i = 0; i < bullets.length; i++) {
    myBullets.push(bullets[i]);
  }

  // get navigation elements
  var navBtns = document.getElementsByClassName("js-data-nav");

  var getDataNav = function() {
    // get attr "data-nav"
    var navBtnAttr = this.getAttribute("data-nav");
    findScreen(navBtnAttr);

    if(navBtnAttr == 'screen5'){
      calculate();
    };
    showScreen(myScreen);
  };
  // listen for clicks
  for(var i = 0; i < navBtns.length; i++) {
    navBtns[i].addEventListener("click", getDataNav, false);
  }

  // find matching screen in fieldsets
  function findScreen(navBtnAttr) {
    var myAttr = navBtnAttr;
    // create arr for fieldset collection
    var myFieldsets = [];
    for (var i = 0; i < fieldsets.length; i++) {
      myFieldsets.push(fieldsets[i]);
    }
    // find screen with id that matches my nav btn attr
    return myScreen = myFieldsets.find(x => x.id === navBtnAttr);
  }

  // show the screen
  function showScreen(myScreen) {
    $('.calculator-screen').hide();
    $(myScreen).fadeIn(300);
    // $(window).scrollTop(0);
    updateBullet(myScreen);
  }

  // activate current bullet
  function updateBullet(myScreen) {
    var myIndex   = myFieldsets.indexOf(myScreen);
    var myBullet  = myBullets[myIndex];
    myPrevBullets = myBullets.slice(0, myIndex);
    myNextBullets = myBullets.slice(myIndex + 1, myIndex.length);
    $(myBullet).removeClass('is-inactive').addClass('is-active');
    $(myPrevBullets).removeClass('is-inactive').addClass('is-active');
    $(myNextBullets).removeClass('is-active').addClass('is-inactive');
  }



 // calculate values for screen5
  function calculate() {
    var h=0;
    var m=0;
    var l =0;
    //n = parseFloat(document.getElementById("numPatientsOnOpioids").value.replace(/\$|\,/g, ''));
    //r = parseFloat(document.getElementById("episodesAvoided").value.replace(/\%|\,/g, ''));
    n = parseFloat(clearSeparators(document.getElementById("numPatientsOnOpioids").value ==''?document.getElementById("numPatientsOnOpioids").placeholder : document.getElementById("numPatientsOnOpioids").value));
    r = parseFloat(clearSeparators(document.getElementById("episodesAvoided").value == ''?document.getElementById("episodesAvoided").placeholder:document.getElementById("episodesAvoided").value));
    
    if(document.getElementById("highRisk").checked){h = 1};
    if(document.getElementById("lowRisk").checked){l = 1};
    if(document.getElementById("mediumRisk").checked){m = 1};
    if(document.getElementById("allPatients").checked){l = 1;m = 1;h = 1;};


    s = parseFloat(clearSeparators(document.getElementById("continuous").value ==''?document.getElementById("continuous").placeholder:document.getElementById("continuous").value));
    i = parseFloat(clearSeparators(document.getElementById("intermittent").value == ''?document.getElementById("intermittent").placeholder:document.getElementById("intermittent").value));
    //console.log("output "+ " n :" +n +" r :"+r +" h :" +h +" l :"+l+" m :"+m +" s :"+s+" i:"+i);
    rdevent = Math.round(n*r/100*(RDL*l+RDM*m+RDH*h));

    los = Math.round(r*n*(LOSL*l+LOSM*m+LOSH*h));

    cost = Math.round(COSTL*r*n*l+COSTM*r*n*m+COSTH*r*n*h-SIL*(s-i)*n*l-SIM*(s-i)*n*m-SIH*(s-i)*n*h);

    document.getElementById("totalEpisodesAvoided").value = rdevent;
    document.getElementById("cumulativeLosReduction").value = los;
    document.getElementById("costReduction").value = '$'+cost.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
  }

  function isNum(str) {
    return /^-?(0|[1-9]\d*)(\.\d+)?$/.test(str);
  }

  function clearSeparators(str) {
    return str.replace(/\$|\%|\,/g, '');
  }

  /* _____________________________________________


  ______________________________________________ */
  const selectedPatientGroups = document.getElementById("selectedPatientGroups");
  const checkSelections = document.getElementById("checkSelections");
  let patientGroup = document.getElementsByClassName("patient-group");
  let myGroups = [];
  let selectedGroups = [];

  for (let i = 0; i < patientGroup.length; i++) {
    myGroups.push(patientGroup[i]);
  }

  function getCheckedVals() {
    // get checked els
    myGroups.forEach(function(el) {
      if (el.checked !== false) {
        selectedGroups.push(el.name);
      }
    });
    // add selected patient groups to final screen
    if(selectedGroups.includes('All patients')){
      selectedPatientGroups.innerHTML = selectedGroups.pop();
    }else{
      // add selected patient groups to final screen
      selectedPatientGroups.innerHTML = selectedGroups.join(',').replace(/,/g, '/').split();
    }
    //reset
    selectedGroups = [];

  }

  checkSelections.addEventListener("click", function() {
    // get checked values and pass to final screen
    getCheckedVals();
  });


//select all checkboxes
$("#allPatients").change(function(){  //"allPatients" change 
  var status = this.checked; // "allPatients" checked status
  $('.patient-group').each(function(){ //iterate all listed checkbox items
    this.checked = status; //change ".patient-group" checked status
  });
});

$('.patient-group').change(function(){ //".patient-group" change 
  //uncheck "allPatients", if one of the listed checkbox item is unchecked
  if(this.checked == false){ //if this item is unchecked
    $("#allPatients")[0].checked = false; //change "allPatients" checked status to false
  }
  
  //check "allPatients" if all checkbox items are checked
  if ($('.patient-group:checked').length == $('.patient-group').length ){ 
    $("#allPatients")[0].checked = true; //change "allPatients checked status to true
  }
});


  /* _____________________________________________

            ACCORDION FUNCTIONS
  ______________________________________________ */
  let toggleTableData = $(".js-toggle-table-data");
  $(".js-table-data").hide();

  $(toggleTableData).on("click", function() {
    let toggleIcon = $(this).children("span");
    let tableData = $(this).next(".js-table-data");
    console.log("this table data: ", tableData);

    if ( $(tableData).hasClass("is-open") ) {
      $(tableData).removeClass("is-open").fadeOut(100);
      $(toggleTableData).css("color", "#53565A");
      $(toggleIcon).removeClass("mi-btb-angle-up").addClass("mi-btb-angle-down").css("color", "#53565A");
    } else {
      $(tableData).addClass("is-open").fadeIn(400);
      $(toggleTableData).css("color", "#0085CA");
      $(toggleIcon).removeClass("mi-btb-angle-down").addClass("mi-btb-angle-up").css("color", "#0085CA");
    }
  });

  const toggleInstructions = document.getElementById("toggleInstructions");
  const instructions = document.getElementById("instructions");

  toggleInstructions.addEventListener("click", function(e) {
    var windowWidth = $(window).width();
    if (windowWidth < 769) {
        $(this).parent().find('#instructions').slideToggle('fast');
        $(this).children('span').toggleClass('is-active');
    }
    e.preventDefault();
  });

  window.addEventListener('resize', function () {
    var windowWidth = $(window).width();
    if (windowWidth >= 769) {
      $(instructions).show();
    } else if (windowWidth < 769) {
      $(instructions).hide();
      $(".mi-btb-angle-down").addClass("is-active");
      $(".mi-btb-angle-up").removeClass("is-active");
    }
  });
});
